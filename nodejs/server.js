var express = require("express");
var app = express();

var server = app.listen(8080, function(){
    console.log("Node.js is listening to PORT:" + server.address().port);
});


var list = [
    { id: "001", name: "photo001", type: "jpg", },
    { id: "002", name: "photo002", type: "jpg", }
]

app.get("/list", function(req, res, next){
    res.json(list);
});

app.get("/message", function(req, res, next){
    console.log(req.query.text);
    res.status(200).send("");
});

function handler(request, response) {
    setTimeout(waited, 2000, request, response);
}

function waited(request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end('Hello World');
}

