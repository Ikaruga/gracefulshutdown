from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

import time

def index(request):
    sleep_time = 10

    print(f"sleeping {sleep_time} seconds.")
    time.sleep(10)

    return HttpResponse("Hello, world. You're at the polls index.")
